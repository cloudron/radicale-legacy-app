#!/bin/bash

set -eu -o pipefail

echo "=> Update radicale config"
sed -e "s/ldap_url = ldap:\/\/.*/ldap_url = ldap:\/\/${LDAP_SERVER}:${LDAP_PORT}/" \
    -e "s/ldap_base = .*/ldap_base = ${LDAP_USERS_BASE_DN}/" \
    "/app/code/config" > "/run/config"

if ! [ -e /app/data/permissions ]; then
    echo "=> Setting default permissions"
    cp /app/code/permissions.default /app/data/permissions
fi

echo "=> Ensure /app/data belongs to cloudron user"
chown -R cloudron:cloudron /app/data /run

echo "=> Start nginx"
nginx -c /app/code/nginx.conf &

echo "=> Exporting data to /tmp/export"
rm -rf /tmp/export
/usr/local/bin/gosu cloudron:cloudron /usr/local/bin/radicale --config /run/config --export-storage /tmp/export/collections
rmdir /tmp/export/collections/collection-root/permissions

echo "=> Start radicale"
exec /usr/local/bin/gosu cloudron:cloudron /usr/local/bin/radicale --debug --config /run/config
